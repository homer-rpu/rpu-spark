set SPARK_RPU_HOME=..

%SPARK_HOME%\bin\spark-submit --class %1 ^
--master local[4] ^
--properties-file %SPARK_RPU_HOME%/conf/spark-defaults.conf ^
%SPARK_RPU_HOME%/lib/rpu-spark-${project.version}.jar ^
%2 ^
%3