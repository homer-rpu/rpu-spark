package com.galerieslafayette.pcm.integration;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructType;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Generate impex for FlagOnline CronJob.
 * <p>
 * Read csv file on input and generate impex  file on output
 * - mode :
 * - PERMISSIVE: tries to parse all lines: nulls are inserted for missing tokens and extra tokens are ignored
 * - DROPMALFORMED: drops lines which have fewer or more tokens than expected or tokens which do not match the schema
 * - FAILFAST: aborts with a RuntimeException if encounters any malformed line
 * </p>
 * Created by p_sfoubert on 10/02/2017.
 */
public class GenerateFlagOnlineImpex {

    private static final Logger LOGGER = Logger.getLogger(GenerateFlagOnlineImpex.class);

    private final static String CSV_SEPARATOR = ";";
    private final static String CSV_EXTENSION = "csv";
    private final static String IMPEX_EXTENSION = "impex";

    private final static String HEADER_IMPEX = "UPDATE RmsArticle; ug[unique = true]; online\n";

    /**
     * Main Execution method
     *
     * @param args the arguments
     */
    public static void main(String[] args) {

        if (args.length != 2) {
            System.out.println("Proper Usage is: spark-submit jarfile inputCsvFile outputImpexFile");
            System.exit(0);
        }

        if (!CSV_EXTENSION.equalsIgnoreCase(FilenameUtils.getExtension(args[0]))) {
            System.out.println("inputCsvFile must be a csv");
            System.exit(0);
        }

        if (!IMPEX_EXTENSION.equalsIgnoreCase(FilenameUtils.getExtension(args[1]))) {
            System.out.println("inputCsvFile must be an impex");
            System.exit(0);
        }

        final String inputCsvFile = args[0];
        final String outputImpexFile = args[1];

        if(!Files.exists(Paths.get(inputCsvFile))){
            System.out.println("inputCsvFile must exist in filesystem");
            System.exit(0);
        }

        long start = System.currentTimeMillis();
        LOGGER.info("[START] GenerateFlagOnlineImpex");
        LOGGER.info("InputCsvFile : " + inputCsvFile);
        LOGGER.info("outputImpexFile : " + outputImpexFile);

        // Start spark session
        SparkSession spark = SparkSession
                .builder()
                .appName("Flag Online impex generator")
                .master("local[4]")
                .getOrCreate();

        // define schema
        StructType schema = new StructType()
                .add("UG", "string")
                .add("TYPE", "string")
                .add("CREATEDDATE", "string")
                .add("STARTDATE", "string")
                .add("ENDDATE", "string")
                .add("PRICE", "double")
                .add("REGPRICE", "double")
                .add("DISCOUNT", "double")
                .add("INTERNETPRICE", "double");

        // Read csv file
        Dataset<Row> df = spark.read()
                .schema(schema)
                .option("header", "true")
                .option("mode", "DROPMALFORMED")
                .option("delimiter", CSV_SEPARATOR)
                .csv(inputCsvFile);

        df.printSchema();

        // Select UG only with positive price
        Date now = Calendar.getInstance().getTime();

        List<Row> rows = df
                .filter(row -> row.getAs("PRICE") != null && (double) row.getAs("PRICE") > 0)
                .select("UG")
                .distinct()
                .collectAsList();

        //Write to impex file
        Path path = Paths.get(outputImpexFile);
        try (BufferedWriter writer = Files.newBufferedWriter(path)) {
            writer.write(HEADER_IMPEX);

            rows.stream()
                    .map(row -> row.getString(0))
                    .forEach(ug -> {
                        logImpexLine(ug);
                        writeToFile(writer, computeImpexLine(ug));
                    });

        } catch (IOException e) {
            LOGGER.error(String.format("Can write to file : %s", path.getFileName()), e);
        } finally {
            spark.close();
        }

        long duration = System.currentTimeMillis() - start;
        LOGGER.info(String.format("[END] GenerateFlagOnlineImpex in %s ms", duration));
    }

    private static void logImpexLine(String ug) {
        if(LOGGER.isDebugEnabled()){
            LOGGER.debug(String.format("Write to impex file : %s", ug));
        }
    }

    /**
     * Write line in file
     *
     * @param writer the buffer writer
     * @param line   the line to write
     */
    private static void writeToFile(BufferedWriter writer, String line) {
        try {
            writer.write(line);
        } catch (IOException e) {
            LOGGER.error(String.format("Can write to file line : %s", line), e);
        }
    }

    /**
     * Generate impex line
     *
     * @param ug the ug
     * @return the ug compute with values
     */
    private static String computeImpexLine(String ug) {
        StringBuilder str = new StringBuilder()
                .append(CSV_SEPARATOR)
                .append(ug)
                .append(CSV_SEPARATOR)
                .append("1\n");
        return str.toString();
    }

}